from menu.models import Menu
from menu.models import MenuItem

from django.contrib import admin

#admin.site.register(Article)
#class ArticleAdmin(admin.ModelAdmin):
#    #fields = ['title','content','pub_date']
#    fieldsets = [
#        (None,           {'fields': ['title','content']}),
#        ('Autoring info',{'fields': ['pub_date','author','category'], 'classes': ['collapse']}),
#    ]

admin.site.register(Menu)
admin.site.register(MenuItem)
