from django.contrib.syndication.views import Feed
from hotspots.models import Hotspot

class HotspotsFeed(Feed):
    title = "Tarragona Wifi. Llistat de punts d\'acces."
    link = "/hotspots/feed/"
    description = "Tarragona Wifi. Llistat de punts d'acces. http://www.tarragonawifi.com"

    def items(self):
        return Hotspot.objects.all().order_by('-id')

    def item_title(self, item):
        return item.name

    def item_link(self,item):
        link = "/hotspots/%d" % item.id
        return link

    def item_description(self, item):
        return item.description
        
        