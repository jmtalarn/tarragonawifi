from django.db import models

from geopy import distance
from geopy.point import Point

import datetime
#from hotspots.models import Hotspot

import logging
logger = logging.getLogger()

class HotspotManager(models.Manager): 
    def getHotspotsByLatAndLong(self,lat,long, dist=5):
      p = Point(lat,long,0)
      d = distance.distance(dist)
      p1 = d.destination(p,45)
      p2 = d.destination(p,225)
      latp1,longp1, _ = p1
      latp2,longp2, _ = p2
      onlong = Hotspot.objects.filter(long__gt=longp2).filter(long__lt=longp1)
      onlat = Hotspot.objects.filter(lat__gt=latp2).filter(lat__lt=latp1)
      results = onlat.filter(id__in=[hotspot.id for hotspot in onlong])
      return results;

    def getLast(self,limit=5):
      return Hotspot.objects.all().order_by('-creation_date','-id')[:limit]

class Hotspot(models.Model):
    name = models.CharField(max_length=100)
    webpage = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    details = models.TextField(blank=True, null=True)
    address = models.CharField(max_length=300, blank=True, null=True)
    creation_date = models.DateTimeField(blank=True,null=True)
    long = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    
    def save(self, *args, **kwargs):
       if self.creation_date is None:
           self.creation_date = datetime.date.today()
       super(Hotspot, self).save(*args, **kwargs) # Call the "real" save() method.
       
    def __unicode__(self):
        return "%s" % self.name
        #return u'%s %s %s,%s' % (self.name,self.address,self.lat,self.long)
    
    objects = HotspotManager()
      