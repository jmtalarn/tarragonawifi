from hotspots.models import Hotspot
from django.contrib import admin
from libraries.massive.actions import export_as_csv_action
from libraries.massive.actions import save_all_action
from django.http import HttpResponse
from search.core import search


#admin.site.register(Article)
#class ArticleAdmin(admin.ModelAdmin):
#    #fields = ['title','content','pub_date']
#    fieldsets = [
#        (None,           {'fields': ['title','content']}),
#        ('Autoring info',{'fields': ['pub_date','author','category'], 'classes': ['collapse']}),
#    ]
 
class HotspotAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields' : ('name','webpage','description','address')
		}),
        ('More details', {
            'classes': ('collapse',),
            'fields': ('details','lat','long','creation_date')
        }),
    )
    list_display = ('name','address','webpage','lat','long')
    actions = [export_as_csv_action(),save_all_action()]
    #class Media:
        #js = (
		    #'http://yui.yahooapis.com/3.4.1/build/yui/yui-min.js',
            #'http://maps.googleapis.com/maps/api/js?sensor=true',
            #'/js/admin/hotspotDetail.js',
			#'/js/admin/hotspotList.js',
            #)
    
    

admin.site.register(Hotspot,HotspotAdmin)
#admin.site.add_action(export_as_csv, 'export_as_csv')
