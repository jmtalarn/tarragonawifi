# Create your views here.
from django.http import HttpResponse

from search.core import search
from hotspots.models import Hotspot

from django.utils import simplejson

from geopy import geocoders
from django.shortcuts import get_object_or_404,render_to_response
from django.template import RequestContext

from libraries.utils.cookie import *

from django.contrib.contenttypes.models import ContentType
from scores.models import Score

from django.conf import settings

import unicodedata


import pprint
import logging
logger = logging.getLogger()

def detail(request, hotspot_id):
    p = get_object_or_404(Hotspot, pk=hotspot_id)
    hotspot_type = ContentType.objects.get_for_model(Hotspot)
    try:
        s = Score.objects.get(content_type__pk=hotspot_type.id,object_id=hotspot_id)
    except Score.DoesNotExist:
        s = Score(object=p,score_pos=0,score_neg=0)
    data = {'hotspot': p, 'score' : s}
     
    if request.is_ajax() or ( ('HTTP_APPID' in request.META) and request.META['HTTP_APPID']=='mobile-tarragonawifi'):
        mimetype = 'application/javascript'
        hotspot = {'id': p.pk, 'name': p.name, 'address': p.address, 'lat': p.lat,'long': p.long, 'description': p.description}
        response_data = {'hotspot' : hotspot, 'score' : {'pos':s.score_pos,'neg':s.score_neg} }
        return HttpResponse(simplejson.dumps(response_data), mimetype)
    else:
        response = render_to_response('hotspots/detail.html', data, context_instance=RequestContext(request))
        set_visitor_id_cookie(request,response)
        return response

def text_search(request):
   param = request.GET.get('q', False)
   qArray = search(Hotspot, param)

   if request.is_ajax() or ( ('HTTP_APPID' in request.META) and request.META['HTTP_APPID']=='mobile-tarragonawifi'):
     mimetype = 'application/javascript'
     results = []
     for h in qArray:
         r = {'id': h.pk, 'name': h.name, 'address': h.address}
         results.append(r)

     data = {'results': results,'search': param}
     return HttpResponse(simplejson.dumps(data), mimetype)

   ## If you want to prevent non XHR calls
   else:
     results = []
     count=0;
     for h in qArray:
         h.page = count / (settings.MAX_PER_PAGE)
         results.append(h)
         count += 1
     max_per_page = len(results) if (len(results)<settings.MAX_PER_PAGE) else settings.MAX_PER_PAGE    
     return render_to_response('hotspots/list.html', {'results': results, 'request':request, 'search': param,'max_per_page': max_per_page})
       # return HttpResponse(status=400)
 
def location_search(request,text_queried):
  g = geocoders.GoogleV3()
  location, (lat,lng) = g.geocode(text_queried.encode("utf-8") + " ,Tarragona")
  if ( not unicodedata.normalize('NFKD', text_queried).encode('ASCII','ignore') in unicodedata.normalize('NFKD', location.split(',')[0]).encode('ASCII','ignore')) or (not "Tarragona" in location.split(',')[1] and not "Spain" in location.split(',')[1] ): 
      location, (lat,lng) = g.geocode("Tarragona ("+ text_queried.encode("utf-8") + " ,Tarragona)")
           
      location = location.split(',')[3] +", "+ location.split(',')[4]
      logger.info("--------------------------- no l'hem trobat")    
  
  logger.info("---------------------------location :"+location)
  results = Hotspot.objects.getHotspotsByLatAndLong(lat,lng,5)  
  if request.is_ajax() or ( ('HTTP_APPID' in request.META) and request.META['HTTP_APPID']=='mobile-tarragonawifi'):
      return latlong_search(request,lat,lng)
  else:
      hotspot_type = ContentType.objects.get_for_model(Hotspot)
      for h in results:
         try:
            score = Score.objects.get(content_type__pk=hotspot_type.id,object_id=h.id)
         except Score.DoesNotExist:
            score = Score(object=h,score_pos=0,score_neg=0)
         h.score = score
 
      response = render_to_response('home/index.html',{'results': results,'lat': lat, 'long': lng,'location': location, 'request':request})
      set_visitor_id_cookie(request,response)
      return response
  
def latlong_search(request,lat,lng):
    results = Hotspot.objects.getHotspotsByLatAndLong(lat,lng,5)
    if request.is_ajax() or ( ('HTTP_APPID' in request.META) and request.META['HTTP_APPID']=='mobile-tarragonawifi'):
        mimetype = 'application/javascript'
        jsonResponse = []
        hotspot_type = ContentType.objects.get_for_model(Hotspot)
        for h in results:
            try:
                score = Score.objects.get(content_type__pk=hotspot_type.id,object_id=h.id)
            except Score.DoesNotExist:
                score = Score(object=h,score_pos=0,score_neg=0)
            s = {'score_pos': score.score_pos,'score_neg': score.score_neg}
            r = {'id': h.pk, 'name': h.name, 'address': h.address, 'lat': h.lat,'long': h.long, 'description': h.description, 'score' : s}
            jsonResponse.append(r)
            
        data = {'results': jsonResponse}
        return HttpResponse(simplejson.dumps( data), mimetype)
        #return HttpResponse(simplejson.dumps(simplejson.loads(serializers.serialize('json', data))),mimetype)    
    else:
        count=0;
        resultsWithPage = []
        for h in results:
          h.page = count / (settings.MAX_PER_PAGE)
          resultsWithPage.append(h)
          count += 1
        max_per_page = len(resultsWithPage) if (len(resultsWithPage)<settings.MAX_PER_PAGE) else settings.MAX_PER_PAGE
        return render_to_response('hotspots/list.html', {'results': resultsWithPage,'request':request,'max_per_page': max_per_page})
 
        

def last_hotspots(request,limit=5):
    lastItems = Hotspot.objects.getLast(limit)
    if request.is_ajax():
        mimetype = 'application/javascript'
        jsonResponse = []
        for h in lastItems:
	    r = {'id': h.pk, 'name': h.name, 'address': h.address, 'lat': h.lat,'long': h.long, 'description': h.description}
	    jsonResponse.append(r)
        data = {'results': jsonResponse}
        return HttpResponse(simplejson.dumps( data), mimetype)
    #return HttpResponse(simplejson.dumps(simplejson.loads(serializers.serialize('json', data))),mimetype)    
    else:
        return render_to_response('hotspots/list.html', {'results': lastItems,'request':request})
  
