from django.conf.urls.defaults import *
from hotspots.feeds import HotspotsFeed

urlpatterns = patterns('hotspots.views',
	url(r'^(?P<hotspot_id>\d+)/$', 'detail'),
	#url(r'^name_search$','name_search'),
	url(r'^search/(?P<lat>-?\d+\.\d+)/(?P<lng>-?\d+\.\d+)/$','latlong_search'),
	#url(r'^search/(?P<text_queried>\w+)/$','location_search'),
	url(r'^search/(?P<text_queried>[\w|\W]+)/$','location_search'),
	url(r'^list/$','text_search'),
	url(r'^last/(?P<limit>\d+)/$','last_hotspots'),
	url(r'^feed/$', HotspotsFeed()),
)
