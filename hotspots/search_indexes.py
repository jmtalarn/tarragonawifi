# post.search_indexes
import search
from search.core import startswith
from hotspots.models import Hotspot

# index used to retrieve hotsposts using the name or the address
search.register(Hotspot, ('name', 'address', ), indexer=startswith)