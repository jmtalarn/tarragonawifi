# -- coding: utf-8 --
# Create your views here.
from django.contrib.contenttypes.models import ContentType
from django.utils import simplejson

from scores.models import Score
from scores.models import Vote

from hotspots.models import Hotspot

from django.http import Http404,HttpResponse
from django.shortcuts import get_object_or_404,render_to_response,redirect
from django.core.exceptions import PermissionDenied

import pprint
import logging
logger = logging.getLogger()

VOTE_DIRECTIONS = (('up', +1), ('down', -1), ('clear', 0))

def vote_on_hotspot(request,vote_direction,hotspot_id):
  response = { 'up': 0, 'down': 0 } # default response
  try:
     vote_score = dict(VOTE_DIRECTIONS)[vote_direction]
  except KeyError:
     raise AttributeError("'%s' is not a valid vote type." % vote_score)
  try:
      h = Hotspot.objects.get(pk=hotspot_id)
      # HTTP_USER_AGENT - The user's browser's user-agent string, if any. This looks something like: "Mozilla/5.0 (X11; U; Linux i686; fr-FR; rv:1.8.1.17) Gecko/20080829 Firefox/2.0.0.17"
      # REMOTE_ADDR - The IP address of the client, e.g., "12.345.67.89". (If the request has passed through any proxies, then this might be a comma-separated list of IP addresses, e.g., "12.345.67.89,23.456.78.90".)
      hotspot_type = ContentType.objects.get_for_model(Hotspot)
      try: 
         s = Score.objects.get(content_type__pk=hotspot_type.id,object_id=hotspot_id)
      except Score.DoesNotExist:
         s = Score(object=h,score_pos=0,score_neg=0,totalscore=0)
         s.save()   
      
      ipaddress = request.META.get('HTTP_X_FORWARDED_FOR','') or request.META.get('REMOTE_ADDR')
      if request.COOKIES.has_key( 'visitor_id' ):
         cookie = request.COOKIES['visitor_id']
      else:
	 cookie = "Cookie not set. Maybe from m.tarragonawifi.com"
	 
      v = Vote(vote_score=vote_score,score=s, 
          ip=ipaddress.split(',')[0],
          useragent = request.META.get('HTTP_USER_AGENT', 'unknown'),
          cookie=cookie)
      v.save()
      #retorna el total positiu o negatiu depenent del sentit del vot
      response = { 'up': v.score.score_pos, 'down': v.score.score_neg}
      if request.is_ajax() or ( ('HTTP_APPID' in request.META) and request.META['HTTP_APPID']=='mobile-tarragonawifi'):
         mimetype = 'application/javascript'
         return HttpResponse(simplejson.dumps(response),mimetype)  
      else:
         return render_to_response('hotspots/detail.html', {'hotspot': h, 'score' : s}, context_instance=RequestContext(request))
  except Hotspot.DoesNotExist:
      if request.is_ajax() or ( ('HTTP_APPID' in request.META) and request.META['HTTP_APPID']=='mobile-tarragonawifi'):
         mimetype = 'application/javascript'
         return HttpResponse(simplejson.dumps(response).mimetype)  
      else:
        raise Http404
       
def clear_votes(request):
  #Requests from the Cron Service will also contain a HTTP header: X-AppEngine-Cron: true
  try:
     # Printar totes les capçaleres
     # logger.info("********************request.META")
     # logger.info(pprint.pprint(request.META))
     if ((request.user and request.user.is_superuser) or request.META['HTTP_X_APPENGINE_CRON']=='true'):
         Vote.objects.all().delete()
         if (request.user and request.user.is_superuser):
            return redirect('/')
         else:
	        return HttpResponse('Operation succeed! All votes were cleared!')
     else:
         logger.error("******* Not super user or X-AppEngine-Cron == 'false'")
         raise PermissionDenied
  except KeyError:
     logger.info("******* Key Error")
     raise PermissionDenied 
