#from django.contrib import admin
#
#class ScoreAdmin(admin.ModelAdmin):
#    pass
#  
#admin.site.register(Score, ScoreAdmin) 

from scores.models import Score
from scores.models import Vote

from django.contrib import admin

class ScoreAdmin(admin.ModelAdmin):
  list_display = ('object', 'score_pos', 'score_neg', 'totalscore')  

class VoteAdmin(admin.ModelAdmin):
  list_display = ('score', 'ip', 'cookie','useragent', 'date','vote_score')  
  
admin.site.register(Score,ScoreAdmin)
admin.site.register(Vote,VoteAdmin)