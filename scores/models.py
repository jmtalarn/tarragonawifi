from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db import IntegrityError
import datetime

import json
#from scores.managers import ScoreManager

SCORES = (
    (u'+1', +1),
    (u'-1', -1),
)

class Score(models.Model):
    """
    A vote on an object by a User.
    """
    content_type = models.ForeignKey(ContentType)
    object_id    = models.PositiveIntegerField()
    object       = generic.GenericForeignKey('content_type', 'object_id')
    score_pos    = models.SmallIntegerField()
    score_neg    = models.SmallIntegerField()
    totalscore   = models.SmallIntegerField()

    def clearVotes(self):
      Vote.objects.filter(score=self).delete()
    
    class Meta:
        # One vote per user per object
        unique_together = (('content_type', 'object_id'),)

    def __unicode__(self):
        return u'%s (id %s) %s votes' % (self.object, self.object_id, self.totalscore)

class ScoreAndVoteEncoder(json.JSONEncoder):
    def default(self, obj):
        if not isinstance(obj, Tree):
            return super(ScoreAndVoteEncoder, self).default(obj)
        return obj.__dict__

class Vote(models.Model):
    vote_score   = models.SmallIntegerField(choices=SCORES)
    score        = models.ForeignKey(Score)
    cookie       = models.CharField(max_length=64)
    ip           = models.IPAddressField()
    useragent    = models.CharField(max_length=200)
    date         = models.DateTimeField(blank=True, null=True)
    
    def save(self, *args, **kwargs):
        if not(Vote.objects.filter(score=self.score,ip=self.ip,useragent=self.useragent,cookie=self.cookie).exists()):
             self.date = datetime.datetime.now()
             super(Vote, self).save(*args, **kwargs) # Call the "real" save() method.
             self.score.totalscore = self.score.totalscore + self.vote_score
             if self.vote_score>0:
                self.score.score_pos = self.score.score_pos + self.vote_score
             else:
                self.score.score_neg = self.score.score_neg + self.vote_score
             self.score.save()
        else:
             raise IntegrityError
         
    class Meta:
        unique_together = (('ip','cookie','useragent'),)

    def __unicode__(self):
        return u'ip: %s with user-agent: %s already voted' % (self.ip, self.useragent)
