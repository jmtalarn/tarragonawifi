from django.conf.urls.defaults import *

urlpatterns = patterns('scores.views',
	url(r'^(?P<vote_direction>\w+)/hotspots/(?P<hotspot_id>\d+)/$', 'vote_on_hotspot'),
	url(r'^clear/votes','clear_votes'),
)