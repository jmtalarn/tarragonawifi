from django.conf.urls.defaults import *

urlpatterns = patterns('home.views',
    url(r'^$', 'home'),
    url(r'^about/', 'about'),
    url(r'^google/ping/', 'reload_sitemap'),
    url(r'^get_update_key/$','get_update_key'),
)
