from django.contrib.sitemaps import Sitemap
import datetime

class HomeSitemap(Sitemap):
    #changefreq = "never"
#changefreq and priority are class attributes corresponding to <changefreq> and <priority> elements, respectively. They can be made callable as functions, as lastmod was in the example.
    def items(self):
        a = ['home','about','hotspots','news']
        return a

    def lastmod(self, obj):
        return datetime.datetime.now()

    def location(self,obj):
        if obj=='home':
           return "/"
        elif obj=='about':
           return "/about"
        elif obj=='hotspots':
           return "/hotspots/list"
        elif obj=='news':
           return "/news/list"  
    #By default, location() calls get_absolute_url() on each object and returns the result.

    def changefreq(self,obj):
        if obj=='home' or obj=='about':
           return "never"
        else:
           return "weekly"

    def priority(self, obj):
        if obj=='home' or obj=='hotspots' or obj=='news':
           return 1.0
        elif obj=='about':
           return 0.5

