# Create your views here.
from django.template import Context, loader
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.sitemaps import ping_google

from django.contrib.admin.models import LogEntry
from django.utils import simplejson

from hotspots.models import Hotspot
from news.models import Article

from django.conf import settings

from libraries.utils.cookie import *

import pprint

ADDITION = 1
CHANGE = 2
DELETION = 3

def home(request):
    t = loader.get_template('home/index.html')
    latestHotspots = Hotspot.objects.getLast(3)
    latestArticles = Article.objects.getLast(3)
    
    c = Context({'request':request,'latestHotspots': latestHotspots, 'latestNews': latestArticles})
    response = HttpResponse(t.render(c))
    set_visitor_id_cookie(request,response)
    return response
    

def about(request):
    t = loader.get_template('about/index.html')
    c = Context({'request':request})
    return HttpResponse(t.render(c))
    
    
def reload_sitemap(request):
    #try:
    ping_google(sitemap_url=settings.SITEMAP_COMPLETE_URL)
    if (request.META['HTTP_X_APPENGINE_CRON']=='true'):
      return HttpResponse('Operation succeed! Sitemap reloaded (Ping to Google Webmasters)!')
    else:  
      return  HttpResponseRedirect('/sitemap.xml')
    #except Exception:
    #  # Bare 'except' because we could get a variety
    #  # of HTTP-related exceptions.
    #  pass

def get_update_key(request):
    last5adminEntries = LogEntry.objects.all().order_by('-action_time')[:5]
    import base64
    r = ""
    for e in last5adminEntries:
        r += base64.b32encode(e.action_time.strftime('%Y%m%d-%H%M')+ e.change_message + e.object_id + e.content_type.name + unicode(e))
    
    if request.is_ajax() or ( ('HTTP_APPID' in request.META) and request.META['HTTP_APPID']=='mobile-tarragonawifi'):
        mimetype = 'application/javascript'
        return HttpResponse(r, mimetype)
    else:
        mimetype = 'application/javascript'
        #return HttpResponse(simplejson.dumps(r), mimetype)
        return HttpResponse(r, mimetype)
 
