# Initialize App Engine and import the default settings (DB backend, etc.).
# If you want to use a different backend you have to remove all occurences
# of "djangoappengine" from this file.
from djangoappengine.settings_base import *

import os

# Uncomment this if you're using the high-replication datastore.
# TODO: Once App Engine fixes the "s~" prefix mess we can remove this.
DATABASES['default']['HIGH_REPLICATION'] = True

# Activate django-dbindexer for the default database
DATABASES['native'] = DATABASES['default']
DATABASES['default'] = {'ENGINE': 'dbindexer', 'TARGET': 'native'}
#AUTOLOAD_SITECONF = 'indexes'
AUTOLOAD_SITECONF = 'search_indexes'

SECRET_KEY = '=r-$b*8hglm+858&9tas^*{}2@hlm6-&6-3d3vfc4((7yd0dbrakhvi'

SITE_ID = 112009 
#SITE_ID = 2304

INSTALLED_APPS = (
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages',
    'djangotoolbox',
    'autoload',
    'dbindexer',
    'search',
    'permission_backend_nonrel',
    # Added in development
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'home',
    #'about',
    'menu',
    'hotspots',
    'news',
    'scores',
    # djangoappengine should come last, so it can override a few manage.py commands
    'djangoappengine',
)
LANGUAGE_CODE = 'ca'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)
MIDDLEWARE_CLASSES = (
    # This loads the index definitions, so it has to come first
    'autoload.middleware.AutoloadMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

LANGUAGE_CODE = 'ca'
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
)

# This test runner captures stdout and associates tracebacks with their
# corresponding output. Helps a lot with print-debugging.
TEST_RUNNER = 'djangotoolbox.test.CapturingTestSuiteRunner'

##############################ADMIN_MEDIA_PREFIX = '/media/admin/'
STATIC_URL = '/'
TEMPLATE_DIRS = (os.path.join(os.path.dirname(__file__), 'templates'),)

ROOT_URLCONF = 'urls'

AUTHENTICATION_BACKENDS = (
    'permission_backend_nonrel.backends.NonrelPermissionBackend',
)

SEARCH_BACKEND = 'search.backends.gae_background_tasks'
#SEARCH_BACKEND = 'search.backends.immediate_update'

#CUSTOM VARIABLES
MAX_PER_PAGE = 10
SITEMAP_COMPLETE_URL = 'http://www.tarragonawifi.com/sitemap.xml'
