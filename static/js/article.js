YUI().use('node','event','overlay','json-parse','io','event-hover', function (Y) {
  Y.on('domready', function () {
      //creacio dels overlays
	TAWI = new Object();
      TAWI.overlay = new Y.Overlay({
	  id: 'detailbox',
	  //fillHeight: true,
	  height: '600px',
	  visible: false,
	  zIndex: 10,
	  constrain: "#mapbox" ,
	  headerContent: "",
	  bodyContent: "",
	  footerContent: "",
	  x: -10,
	  y: -10,
	  //...
	});
	TAWI.overlay.clickclose = function(){
	  Y.one('div.social-buttons').setStyle('display','none');
	  TAWI.points.unselectAll();
	  TAWI.overlay.hide();
	};
	TAWI.overlay.render();
	
	//Loading overlay
	TAWI.loading = new Object();
	TAWI.loading.map = new Y.Overlay({
	  id: 'loadingmapbox',
	  fillHeight: true,
	  visible: false,
	  zIndex: 10,
	  constrain: "#mapbox" ,
	  headerContent: "",
	  bodyContent: '<img src="/img/loading.gif">',
	  footerContent: "",
	});
	TAWI.loading.map.render();
	TAWI.loading.map.set("centered", "#mapbox");
	TAWI.loading.article = new Y.Overlay({
	  id: 'loadingarticlebox',
	  fillHeight: true,
	  visible: false,
	  zIndex: 10,
	  constrain: "#articlebox" ,
	  headerContent: "",
	  bodyContent: '<img src="/img/loading.gif">',
	  footerContent: "",
	});	
	TAWI.loading.article.render();
	TAWI.loading.article.set("centered", "#articlebox");
	TAWI.loading.show = function(){
	  TAWI.loading.article.show();
	  TAWI.loading.map.show();
	  YUI().use('node', function (Y) {
	    Y.one("#mapbox").setStyle('opacity',0.20);
	    Y.one("#articlebox").setStyle('opacity',0.20);
	  });
	};
	TAWI.loading.hide = function(){
	  YUI().use('node', function (Y) {
	    Y.one("#mapbox").setStyle('opacity',1.00);
	    Y.one("#articlebox").setStyle('opacity',1.00);
	  });
	  TAWI.loading.article.hide();
	  TAWI.loading.map.hide();	  
	};
	
      //posicionament del mapa
	 var lat = Y.one("span#lat").get("textContent");
	 var long = Y.one("span#long").get("textContent");
	 //var name = Y.one("div#name").get("textContent");
	latlng = new google.maps.LatLng(lat, long); 
	 
	var latlng = new google.maps.LatLng(lat, long); //Latitud i Longitud de Tarragona
	var myOptions = {
		zoom: 14,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	 
	 ////var myLatlng = new google.maps.LatLng(lat,long);
 	 TAWI.map = new google.maps.Map(document.getElementById("mapbox"),  myOptions);
      //Cerca dels punts i pintar-los sobre el mapa
	TAWI.loading.show();	
  	var callUrl = '/hotspots/search/'+ lat+'/'+ long+'/';
    	Y.io(callUrl, {
    	    on : {
    	        success : function (tx, r) {
    	            var parsedResponse;

    	            // protected against malformed JSON response
    	            try {
    	               parsedResponse = Y.JSON.parse(r.responseText);
    	            }
    	            catch (e) {
    	                alert("JSON Parse failed!");
    	                return;
		    }
		    if (!Y.Lang.isUndefined(TAWI.points)){
			    for (var i=0;i<TAWI.points.length;i++){
			      TAWI.points[i].setMap(null)
			    }
		    }
		    TAWI.points = [];
		    TAWI.pin = new Object();
		    TAWI.pin.normal = '/img/pin-logo.png';
		    TAWI.pin.glow = '/img/pin-logo-glow.png';
		    
		    for (var i=0; i < parsedResponse.results.length; i++) {
			var hotspot = parsedResponse.results[i];
			var hsLatlng = new google.maps.LatLng(hotspot.lat,hotspot.long);
			var marker = new google.maps.Marker({
			  position: hsLatlng,
			  map: TAWI.map,
			  title: hotspot.name,
			  icon: TAWI.pin.normal
			});
			marker.idx = i;
			marker.hotspot = new Object();
			marker.hotspot.id = hotspot.id;
			marker.hotspot.name = hotspot.name;
			marker.hotspot.description = hotspot.description;
			marker.hotspot.socialHTML  = '<div class="social-buttons article">';
			marker.hotspot.socialHTML += '<ul><li>';
			marker.hotspot.socialHTML += '<div class="fb-like" data-href="http://www.tarragonawifi.com/hotspots/'+hotspot.id+'" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>';
			marker.hotspot.socialHTML += '</li><li>';
			marker.hotspot.socialHTML += '<a href="https://twitter.com/share" data-url="http://www.tarragonawifi.com/hotspots/'+hotspot.id+'" data-text="'+hotspot.name+' is a hotspot in " data-via="tarragonawifi" data-hashtags="tarragonawifi" data-count="horizontal" data-lang="en" class="twitter-share-button">Tweet</a>';
			marker.hotspot.socialHTML += '</li><li>';
			marker.hotspot.socialHTML += '<g:plusone size="medium" annotation="inline" href="http://www.tarragonawifi.com/hotspots/'+hotspot.id+'" width="120"></g:plusone>';
			marker.hotspot.socialHTML += '</li></ul>';
			marker.hotspot.socialHTML += '</div>';
			
			google.maps.event.addListener(marker, 'click', function() {
			  for (var i=0;i<TAWI.points.length;i++){
			      TAWI.points[i].setIcon(TAWI.pin.normal)
			  }
			TAWI.overlay.set("headerContent",this.hotspot.name + '<a class="hide" href="#content" style="font-size: 0.8em;position:relative;float: right;" title="close"><img class="close_icon" src="/img/close_icon_out.png"></a>' );
			var overFn = function(){
			    this.set('src','/img/close_icon.png');
			};
			var outFn = function(){
			    this.set('src','/img/close_icon_out.png');
			};
			var elDiv = Y.one("div#mapbox");
			Y.on("hover", overFn, outFn, "img.close_icon");
			TAWI.overlay.set("bodyContent",this.hotspot.description );
			TAWI.overlay.set("footerContent",this.hotspot.socialHTML );
			TAWI.overlay.set("width",elDiv.get("offsetWidth"));
			TAWI.overlay.set("height",elDiv.get("offsetHeight"));
			TAWI.overlay.render();
			Y.on("click", Y.bind(TAWI.overlay.clickclose, TAWI.overlay), ".hide");
			TAWI.overlay.show();

			this.setIcon(TAWI.pin.glow);
			this.getMap().panTo(TAWI.points[this.idx].getPosition());
			socialize_it();
		      });
		      
		      TAWI.points.push(marker);
		};
		TAWI.points.unselectAll = function(){
		  for (var i=0;i<TAWI.points.length;i++){
			  TAWI.points[i].setIcon(TAWI.pin.normal)
		  }
		}

		TAWI.loading.hide();
		socialize_it();
	    },
	    failure : function (x,o) {
		alert('Failed call! Please retry, if problem persists feed back the site admin.');
		TAWI.loading.hide();
	    }
	}
   	});

	  
  });
}); 
