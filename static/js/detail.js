
YUI().use('node','event','io-base','json-parse', function (Y) {
  Y.on('domready', function () {
	//*************************Marker on Map***************************************
	var lat = Y.one("span#lat").get("textContent");
	var long = Y.one("span#long").get("textContent");
	var name = Y.one("span#name").get("textContent");
	 
	TAWI = new Object();
	TAWI.pin = new Object();
	TAWI.pin.glow = '/img/pin-logo-glow.png';
	 
	var latlng = new google.maps.LatLng(lat, long); //Latitud i Longitud de Tarragona
	var myOptions = {
		zoom: 15,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	 
	var myLatlng = new google.maps.LatLng(lat,long);
 	TAWI.map = new google.maps.Map(document.getElementById("mapbox"),  myOptions);

	var marker = new google.maps.Marker({
		position: latlng,
		map: TAWI.map,
		title: name,
		icon: TAWI.pin.glow
	});

  });

});