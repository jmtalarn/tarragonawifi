﻿django.jQuery(document).ready(function() {	

	var div2Add = '<div class="autocomplete">'+
	'<img src="/admin/img/icon_searchbox.png" alt="Search" />'+
	'<input type="text" name="autocompleteInput" id="autocompleteInput" class="typeahead"/>'+
	
  '</div>'+
  '<div style="clear: right;"></div>';
  django.jQuery(".object-tools").append(div2Add);
  $('#autocompleteInput').autocomplete({
      source: function(request, response) {
          $.getJSON("/hotspots/list", { q: request.term }, function(data){
        	  response(data.results);
          }); 
      },	  
      minLength: 3,
      delay: 500,
      messages: {
          noResults: '',
          results: function(numResults) {  return numResults + " results found.";}
      },
      select: function( event, ui ) {
    	  window.location = '/admin/hotspots/hotspot/' + ui.item.id;
      }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li>" )
      .append( "<a><b>" + item.name + "</b><br>" + item.address + "</a>" )
      .appendTo( ul );
  };

});
