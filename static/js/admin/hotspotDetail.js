﻿django.jQuery(document).ready(function() {

 //django.jQuery('#id_long')[0].disabled=true;
 //django.jQuery('#id_lat')[0].disabled=true;
 
 django.jQuery('#id_address').parent().append("<div id='map_canvas' style='width:380px; height:200px; margin: 20px; margin-left: 105px;' ></div>");
 
  var latlng = new google.maps.LatLng(41.1166667, 1.25); //Latitud i Longitud de Tarragona
  var myOptions = {
    zoom: 15,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
   
   
  var map = new google.maps.Map(document.getElementById("map_canvas"),  myOptions);
 
 
  django.jQuery('#id_address').change(function() {
    updateMapPosition(map,true);
  });
 
  // on load update map if address is not empty
  if (django.jQuery('#id_lat').val() && django.jQuery('#id_long').val()) {
    updateMapPosition(map,false);
  }else if (django.jQuery('#id_address').val()) {
    updateMapPosition(map,true);
  }
  
  $('#id_details').wysihtml5();
  $('#id_description').wysihtml5();
});
var marker;  

function updateMapPosition(map,address) {
  var geocoder = new google.maps.Geocoder();
  var config = {};
  if (address){
     config = {'address': django.jQuery('#id_address').val()} ;
  }else{
    var latLng = new google.maps.LatLng( django.jQuery('#id_lat').val(),
                                         django.jQuery('#id_long').val());
    config = { 'latLng' : latLng}; 
  } 
  var position = geocoder.geocode(config ,
  function(results,status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
        map.setCenter(results[0].geometry.location);
        marker = new google.maps.Marker({map:map, position:results[0].geometry.location, draggable: true});
        django.jQuery('#id_long')[0].value=marker.position.lng();
        django.jQuery('#id_lat')[0].value=marker.position.lat();

        google.maps.event.addListener(marker, "dragend", function(latlng) {
    		django.jQuery('#id_long')[0].value=latlng.latLng.lng();
    		django.jQuery('#id_lat')[0].value=latlng.latLng.lat();
  		}); 
        
      }
    }else{
        alert("Adresse inconnue par google map, rajoutez le code postal et le pays.");
    }
  });
}



