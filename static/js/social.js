function load_social_capabilities(){
     try{
		!function(d,s,id){
		var js,fjs=d.getElementsByTagName(s)[0];
		if(!d.getElementById(id)){
		js=d.createElement(s);
		js.id=id;
		js.src="http://platform.twitter.com/widgets.js";
		fjs.parentNode.insertBefore(js,fjs);}
		}(document,"script","twitter-wjs");
     }catch(err){
		console.warn(err+". Page not Twittered");
     }
     try{
		(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "http://connect.facebook.net/en_US/all.js#xfbml=1";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
     }catch(err){
		YUI().use('console', function (Y) {
			Y.log(err+". Page not Facebooked", "warn",  "Tarragona Wifi");
		});
     }	
     try{
		window.___gcfg = {lang: 'en'};
		(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/plusone.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();
     }catch(err){
		YUI().use('console', function (Y) {
			Y.log(err+". Page not GooglePlused","warn", "Tarragona Wifi");
		});
     }	
};

function socialize_it(){
    //re-render twitter icons by calling the render() method on each
    try{
    	twttr.widgets.load()
    }catch(err){
		YUI().use('console', function (Y) {
			Y.log(err+". Page not Twittered", "warn", "Tarragona Wifi");
		});
    }
    try{
		gapi.plusone.go();
    }catch(err){
		YUI().use('console', function (Y) {
			Y.log(err+". Page not GooglePlused","warn", "Tarragona Wifi");
		});
    }
    try{
		FB.XFBML.parse(document.getElementById('listbox'));
    }catch(err){
		YUI().use('console', function (Y) {
			Y.log( err+". Page not Facebooked","warn", "Tarragona Wifi");
		});
    }	
};
