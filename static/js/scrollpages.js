YUI().use('scrollview','scrollview-paginator','event','node', function (Y) {
	 
	Y.on('domready', function () {
		var max = 0;
		var pageWidths = Y.all('li.page').getComputedStyle('width');
		for (a in pageWidths){
			if (max<parseFloat(pageWidths[a].replace('px',''))){ 
				max = parseFloat(pageWidths[a].replace('px',''));
			}
		};
		
		
		theScrollview = new Y.ScrollView({
			id: "scrollview",
			srcNode:"#scrollview-content",
			/*height: "60em",*/
			width: max,
			flick: {
				minDistance:10,
				minVelocity: 0.3,
				axis: "x"
			}
		});

		
		theScrollview.plug(Y.Plugin.ScrollViewPaginator, {selector: "li.page"});
		theScrollview.maxPerPage = parseInt(Y.one('span#max_per_page').getContent());
		
		Y.all('button.topage').on('click', function(e){
			var pageToGo = parseInt(e.target.get("id").replace("topage_",""))-1;
			var pageNumber = pageToGo +1 ;
			var itemsOnPage = Y.all('li#page_'+pageNumber+" ul li.item");
			Y.all("span.itemFrom").setContent((pageToGo*theScrollview.maxPerPage)+1);	//setContent pageToGo*20
			Y.all("span.itemTo").setContent(pageToGo*theScrollview.maxPerPage + itemsOnPage.size()); //setContent  pageToGo*20 + itemsOnPage.size() 
			theScrollview.pages.scrollTo(pageToGo, 0.6, "ease-in");
		});
		theScrollview.render();
		/*scrollview.pages.set("index",3);
		scrollview.pages.scrollTo(3, 0.6,"ease-in");
		scrollview.pages.next();
		scrollview.pages.get("total");*/
	});
});
