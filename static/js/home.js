﻿/* home.js */
TAWI = new Object();
TAWI.search = new Object();
TAWI.points = [];
TAWI.points.selected = null;

TAWI.pin = new Object();
TAWI.pin.normal = '/img/pin-logo.png';
TAWI.pin.glow = '/img/pin-logo-glow.png';

TAWI.points.unselectAll = function(){
  TAWI.points.selected = null;
  for (var i=0;i<TAWI.points.length;i++){
	  TAWI.points[i].setIcon(TAWI.pin.normal)
  }
};

TAWI.points.clickMarker = function() {
	markerClicked = this;
	YUI(markerClicked).use('node','event',function(Y){
	    TAWI.points.unselectAll();
	    //	for (var i=0;i<TAWI.points.length;i++){
	    //		TAWI.points[i].setIcon(TAWI.pin.normal)
	    //	}
	    //Y.all('li.list_item').removeClass('highlight');
	    var elDiv = Y.one('div#listbox');
	    var scrollHeight = elDiv.get('scrollHeight');
	    var unitScroll = (scrollHeight / TAWI.points.length);
	    var elNum = markerClicked.li_id.replace("#list_item","");
	    TAWI.points.selected = Y.one("div#listbox li"+markerClicked.li_id);
	    elDiv.set('scrollTop',unitScroll*elNum);

	    TAWI.overlay.set("headerContent",Y.one(markerClicked.li_id + ' span.name').getContent() + '<a class="hide" href="#mapbox" style="font-size: 0.8em;position:relative;float: right;" title="close"><img class="close_icon" src="/img/close_icon_out.png"></a>' 
	    			+ Y.one(markerClicked.li_id + ' div.social-buttons-parent').getContent() + Y.one(markerClicked.li_id + ' div.scoring-parent').getContent());
	    var overFn = function(){
	      this.set('src','/img/close_icon.png');
	    };
	    var outFn = function(){
	      this.set('src','/img/close_icon_out.png');
	    }
	    Y.on("hover", overFn, outFn, "img.close_icon");
	    TAWI.overlay.set("bodyContent",Y.one(markerClicked.li_id + ' div.more_info').getContent() );
	    TAWI.overlay.set("footerContent", "");
	    TAWI.overlay.set("width",elDiv.get("offsetWidth"));
	    TAWI.overlay.set("height",elDiv.get("offsetHeight"));
	    TAWI.overlay.render();
	    Y.on("click", Y.bind(TAWI.overlay.clickclose, TAWI.overlay), ".hide");
	    TAWI.thumbs.initThumbs(TAWI.points.selected.one("span#id").getContent());
	    TAWI.overlay.show();

	    markerClicked.setIcon(TAWI.pin.glow);
	});
};
TAWI.points.listItemClick = function(e) {
	YUI().use('node','event',function(Y){
	  TAWI.points.unselectAll();
	  //Y.all('li.list_item').removeClass('highlight');
	  var elDiv = Y.one('div#listbox');
	  var scrollHeight = elDiv.get('scrollHeight');

	  var elLi = e.target;
	  if (elLi.get("nodeName")=="SPAN"){
		  elLi = elLi.get("parentNode");
	  }
	  var elNum = elLi.get('id').replace("list_item","");
	  TAWI.points.selected = elLi;
	  //elLi.addClass('highlight');
	  TAWI.overlay.set("headerContent",Y.one('#'+elLi.get('id') + ' span.name').getContent() + '<a class="hide" href="#mapbox" style="font-size: 0.8em;position:relative;float: right;" title="close"><img class="close_icon" src="/img/close_icon_out.png"></a>' 
			  				+ Y.one('#'+elLi.get('id') + ' div.social-buttons-parent').getContent() + Y.one('#'+elLi.get('id') + ' div.scoring-parent').getContent());
	  var overFn = function(){
		  this.set('src','/img/close_icon.png');
	  };
	  var outFn = function(){
		  this.set('src','/img/close_icon_out.png');
	  };
	  Y.on("hover", overFn, outFn, "img.close_icon");
	  TAWI.overlay.set("bodyContent",Y.one('#'+elLi.get('id') + ' div.more_info').getContent() );
	  TAWI.overlay.set("footerContent","" );
	  TAWI.overlay.set("width",elDiv.get("offsetWidth"));
	  TAWI.overlay.set("height",elDiv.get("offsetHeight"));
	  TAWI.overlay.render();
	  Y.on("click", Y.bind(TAWI.overlay.clickclose, TAWI.overlay), ".hide");
	  TAWI.thumbs.initThumbs(Y.one('#'+elLi.get('id') + ' span#id').getContent());
	  TAWI.overlay.show();

	  TAWI.points[elNum].setIcon(TAWI.pin.glow);
	  TAWI.points[elNum].getMap().panTo(TAWI.points[elNum].getPosition());
  });
};
YUI().use('node','event','overlay', function (Y) {
	Y.on('domready', function () {
		TAWI.overlay = new Y.Overlay({
		  id: 'detailbox',
		  fillHeight: true,
		  visible: false,
		  zIndex: 10,
		  constrain: "#listbox" ,
		  headerContent: "",
		  bodyContent: "",
		  footerContent: "",
		  x: -10,
		  y: -10,
		  //...
		});
		TAWI.overlay.clickclose = function(){
		  Y.one('div.social-buttons').setStyle('display','none');
		  var score_pos = Y.one("div#detailbox div#scoring.scoring span#score_pos.thumbtext").getContent();
		  var score_neg = Y.one("div#detailbox div#scoring.scoring span#score_neg.thumbtext").getContent();
		  
		  TAWI.points.selected.one("span#score_pos.thumbtext").setContent(score_pos);
		  TAWI.points.selected.one("span#score_neg.thumbtext").setContent(score_neg);
		  
		  TAWI.points.unselectAll();
		  TAWI.overlay.hide();
		};
		TAWI.overlay.render();
	
		//Loading overlay
		TAWI.loading = new Object();
		TAWI.loading.map = new Y.Overlay({
		  id: 'loadingmapbox',
		  fillHeight: true,
		  visible: false,
		  zIndex: 10,
		  constrain: "#mapbox" ,
		  headerContent: "",
		  bodyContent: '<img src="/img/loading.gif">',
		  footerContent: "",
		});
		TAWI.loading.map.render();
		TAWI.loading.map.set("centered", "#mapbox");
		TAWI.loading.list = new Y.Overlay({
		  id: 'loadinglistbox',
		  fillHeight: true,
		  visible: false,
		  zIndex: 10,
		  constrain: "#listbox" ,
		  headerContent: "",
		  bodyContent: '<img src="/img/loading.gif">',
		  footerContent: "",
		});	
		TAWI.loading.list.render();
		TAWI.loading.list.set("centered", "#listbox");
		TAWI.loading.show = function(){
		  TAWI.loading.list.show();
		  TAWI.loading.map.show();
		  YUI().use('node', function (Y) {
			Y.one("#mapbox").setStyle('opacity',0.20);
			Y.one("#listbox").setStyle('opacity',0.20);
		  });
		};
		TAWI.loading.hide = function(){
		  YUI().use('node', function (Y) {
			Y.one("#mapbox").setStyle('opacity',1.00);
			Y.one("#listbox").setStyle('opacity',1.00);
		  });
		  TAWI.loading.list.hide();
		  TAWI.loading.map.hide();	  
		};
	});
});

function updateResults(lat,long){
  var cfg = { 'lat': lat, 'lng' : long };

  YUI(cfg).use('node','json-parse','io','event-hover', function (Y) {
    	TAWI.loading.show();	
  	var callUrl = '/hotspots/search/'+ cfg.lat+'/'+ cfg.lng+'/';    		
    	Y.io(callUrl, {
	on : {
	  success : function (tx, r) {
	  //var parsedResponse = new Object();

	  //// protected against malformed JSON response
	  //try {
	      var parsedResponse = Y.JSON.parse(r.responseText);
	  //}
	  //catch (e) {
	  //    alert("JSON Parse failed!");
	  //    return;
		      //}
		var target = Y.one('div#listbox');
		var html ='<ul class="hotspots">';
		if (!Y.Lang.isUndefined(TAWI.points)){
			for (var i=0;i<TAWI.points.length;i++){
			  TAWI.points[i].setMap(null)
			}
		}
	
		for (var i=0; i < parsedResponse.results.length; i++) {
		  var hotspot = parsedResponse.results[i];
		  var li_id = 'list_item'+i;
		  html += '<li id="'+li_id+'" class="list_item"><span class="name">' + hotspot.name+'</span><span id="id" style="display:none;">'+hotspot.id+'</span>';
		  html += '<div class="scoring-parent">';
		  html += '<div class="scoring home" id="scoring">';
		  html += '<div title="Plena satisfacció! Pica aquí per donar-li suport" class="thumb up"><span id="score_pos" class="thumbtext">'+hotspot.score.score_pos+'</span></div>';
		  html += '<div title="Fracàs estrepitós! Pica aquí per donar l\'avis" class="thumb down"><span id="score_neg" class="thumbtext">'+hotspot.score.score_neg+'</span></div>';
		  html += '</div>';
		  html += '</div>';
		  html += '<div class="social-buttons-parent">';
		  html += '<div class="social-buttons home">';
		  html += '<ul><li>';
		  html += '<a href="https://twitter.com/share" data-url="http://www.tarragonawifi.com/hotspots/'+hotspot.id+'" data-text="'+hotspot.name+' is a hotspot in " data-via="tarragonawifi" data-hashtags="tarragonawifi" data-count="horizontal" data-lang="en" class="twitter-share-button">Tweet</a>';
		  html += '</li><li>';
		  html += '<g:plusone size="medium" annotation="inline" href="http://www.tarragonawifi.com/hotspots/'+hotspot.id+'" width="120"></g:plusone>';
		  html += '</li><li>';
		  html += '<div class="fb-like" data-href="http://www.tarragonawifi.com/hotspots/'+hotspot.id+'" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>';
		  html += '</li></ul>';
		  html += '</div></div>';
		  html += '<div class="more_info">' + hotspot.description+'</div>';
		  html += '</li>';
		  var myLatlng = new google.maps.LatLng(hotspot.lat,hotspot.long);
		  var marker = new google.maps.Marker({
			position: myLatlng,
			map: TAWI.map,
			title: hotspot.name,
			icon: TAWI.pin.normal
		  });
		  google.maps.event.addListener(marker, 'click', TAWI.points.clickMarker);
		  marker.li_id = '#'+li_id;
		  TAWI.points.push(marker);
		};

		html +='</ul>';
		target.setContent(html);

		target.delegate('click', TAWI.points.listItemClick, 'li');
		socialize_it();
		TAWI.loading.hide();
		},
		failure : function (x,o) {
			var target = Y.one('div#listbox');
			target.setContent('Failed call! Please retry, if problem persists feed back the site admin.');
			TAWI.loading.hide();
		}
	      }
	  });
    });
}
    function updateMapPosition(map,Y) {
      var geocoder = new google.maps.Geocoder();
      var config = {};
      window.location='#inputsearch';
      config = {'address': Y.one('#inputsearch').get('value')} ;

      var position = geocoder.geocode(config , function(results,status) {
	if (status == google.maps.GeocoderStatus.OK) {
	  if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
	    TAWI.map.setCenter(results[results.length-1].geometry.location);
	    if (!Y.Lang.isUndefined(TAWI.search.marker)){
		    TAWI.search.marker.setMap(null);
	    }
	    TAWI.search.marker = new google.maps.Marker({map:TAWI.map, position:results[results.length-1].geometry.location, draggable: true});
	    updateResults(TAWI.search.marker.position.lat(),TAWI.search.marker.position.lng());
	    position = TAWI.search.marker.position;
	    //Aqui va la crida Ajax: AjaxCall(position.lng(),position.lat())
	    google.maps.event.addListener(TAWI.search.marker, "dragend", function(position) {
	    updateResults(position.latLng.lat(),position.latLng.lng());
	  }); 
	}
      }else{
	alert("Adreça desconeguda! Intenta donar més informació.");
      }});
    };

YUI().use('node','event', function (Y) {
  Y.on('domready', function () {
	TAWI.loading.show();
	Y.one('#inputsearch').on('change', function () {
		updateMapPosition(TAWI.map,Y);
	});
	var latlng = new google.maps.LatLng(41.1166667, 1.25); //Latitud i Longitud de Tarragona	
	if (Y.one('span#location')){
		var lat = Y.one("span#location_lat").get("textContent");
	 	var long = Y.one("span#location_long").get("textContent");
		latlng = new google.maps.LatLng(lat, long); //Latitud i Longitud de la consulta
		Y.one('#inputsearch').set("value",Y.one('span#location').get("textContent"));
	}	
	var myOptions = {
		zoom: 15,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	//load_social_capabilities();
	
	TAWI.map = new google.maps.Map(document.getElementById("mapbox"),  myOptions);
	if (Y.one('span#location')){
		//Agafar els elements i crear els punts al mapa
		var listItems = Y.all('ul.hotspots li.list_item');		
		listItems.each(function (item) {
			  var lat = item.one('span#lat').getContent();
			  var lng = item.one('span#long').getContent();
   			  var name = item.one('span#name').getContent();
			  var myLatlng = new google.maps.LatLng(lat,lng);
		      var marker = new google.maps.Marker({
				position: myLatlng,
				map: TAWI.map,
				title: name,
				icon: TAWI.pin.normal
		      });
  	      google.maps.event.addListener(marker, 'click', TAWI.points.clickMarker);
		  marker.li_id = item.get("id");
		  TAWI.points.push(marker);
		  
          Y.one('div#listbox').delegate('click', TAWI.points.listItemClick, 'li');
		
		});
	};
	TAWI.loading.hide();	
	
  });
});

function showPopup(url) {
	newwindow=window.open(url,'name','height=225,width=520,top=200,left=300,resizable');
	if (window.focus) {newwindow.focus()}
}
