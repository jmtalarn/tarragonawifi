
YUI().use('node','event','io-base','json-parse', function (Y) {
  Y.on('domready', function () {
	TAWI.thumbs = {};
	TAWI.thumbs.initThumbs = function(hotspot_id){
	  TAWI.thumbs.up = { div: Y.one('div.thumb.up'), span: Y.one('span#score_pos'), uri: "/thumb/up/hotspots/" + hotspot_id +"/"};
	  TAWI.thumbs.down = { div: Y.one('div.thumb.down'), span: Y.one('span#score_neg'), uri: "/thumb/down/hotspots/" + hotspot_id + "/" };
	  TAWI.thumbs.success = function(id, o, args){ 
	      var id = id; // Transaction ID.
	      var data = Y.JSON.parse(o.responseText); // Response data.

	      TAWI.thumbs[args.direction].span.setContent(data[args.direction]);
	  };
	  TAWI.thumbs.failure = function(id, o, args) {
	      alert("Impossible registrar el vot.\nNo pots votar un punt més d'un cop al dia.\nIntenta-ho més endavant."); 
	  };
	  //***********************http://yuilibrary.com/yui/docs/io/
	  TAWI.thumbs.cfg = {
	    method: 'POST',
	    on: {
		success: TAWI.thumbs.success,
		failure: TAWI.thumbs.failure,
	    },
	  // form: {
	  //   ip: formObject, //EM SEMBLA QUE AIXÒ S'HAURIA DE RECUPERAR AL SERVIDOR SI S'ENVIA LA REQUEST
	  //   useragent: true, //EM SEMBLA QUE AIXÒ S'HAURIA DE RECUPERAR AL SERVIDOR SI S'ENVIA LA REQUEST
	  // },
	    arguments: {
	      direction: ''
	    }
	  };
	  //*******Listeners on thumb up and thumb down *********************************	  
	  TAWI.thumbs.up.vote = function(e){
	    TAWI.thumbs.cfg.arguments.direction = 'up';  
	    var request = Y.io(TAWI.thumbs.up.uri,TAWI.thumbs.cfg);
	  };
	  TAWI.thumbs.up.div.on('click',TAWI.thumbs.up.vote);
	  TAWI.thumbs.down.vote = function(e){
	      TAWI.thumbs.cfg.arguments.direction = 'down'; 
	      var request = Y.io(TAWI.thumbs.down.uri,TAWI.thumbs.cfg);
	  };
	  TAWI.thumbs.down.div.on('click',TAWI.thumbs.down.vote);
	}
	var spanId = Y.one("span#id");
	if (!Y.Lang.isNull(spanId)){
	  TAWI.thumbs.initThumbs(spanId.get("textContent"));
	}
   	
	
 
  });
}); 
