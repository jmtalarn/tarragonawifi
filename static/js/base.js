String.prototype.trim = function(){ return (this.replace(/^[\s\xA0]+/, "").replace(/[\s\xA0]+$/, ""))}
String.prototype.startsWith = function(str){return (this.match("^"+str)==str)}
String.prototype.endsWith = function(str){return (this.match(str+"$")==str)}

YUI().use("node", function(Y) {

	if (screen.width <= 960) {
		if (confirm("Hem detectat que accedeix des d\'un dispositiu de reduides dimensions.\nVol accedir a la nova versió mòbil de Tarragona Wifi?")){
		  var params = location.pathname;
		  var url_to_go = "http://m.tarragonawifi.com";
		  if (params.startsWith("/hotspots/search/")){
		    url_to_go += "/hotspot/search/location?q="+params.split("/hotspots/search/")[1].substring(0, params.split("/hotspots/search/")[1].length-1)
		  }else if (params.startsWith("/hotspots/list/")){
		    url_to_go += "/hotspot/search/text" + location.search;
		  }else if (params.match(/\/hotspots\/[0-9]+/)!=null ){
		    url_to_go += params.replace("hotspots","hotspot")
		  }
		  window.location = url_to_go;
		}else{
			// Do Nothing
		}

	}

	Y.on("domready", function () {
		load_social_capabilities();

    });
});


function getUrlParts(url) {
    var a = document.createElement('a');
    a.href = url;

    return {
        href: a.href,
        host: a.host,
        hostname: a.hostname,
        port: a.port,
        pathname: a.pathname,
        protocol: a.protocol,
        hash: a.hash,
        search: a.search
    };
}
