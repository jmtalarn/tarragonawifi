YUI().use('node','event', function (Y) {
	 Y.on('domready', function () {
		var inputsearch = Y.one('#inputsearch');
		if (inputsearch.get('value')=='' || inputsearch.get('value')=='Escriu aquí per buscar...'){
			inputsearch.addClass('notext');
			inputsearch.set('value','Escriu aquí per buscar...');
		}
				
		inputsearch.once('click', function () {
			this.removeClass("notext");
			this.set('value',"");
		});
	 });
});
