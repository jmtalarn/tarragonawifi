import csv
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied

def save_all_action(description="Save all objects"):
    def save_all(modeladmin,request,queryset):
        for obj in queryset:
            obj.save()
    save_all.short_description=description
    return save_all    
      
def export_as_csv_action(description="Export selected objects as CSV file"):
    def export_as_csv(modeladmin, request, queryset):
        """
        Generic csv export admin action.
        """
        if not request.user.is_staff:
            raise PermissionDenied
        opts = modeladmin.model._meta
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % unicode(opts).replace('.', '_')
        writer = csv.writer(response)
        field_names = [field.name for field in opts.fields]
        # Write a first row with header information
        writer.writerow(field_names)
        # Write data rows
        for obj in queryset:
            writer.writerow([unicode(getattr(obj, field)).encode("utf-8","replace") for field in field_names])
        return response
    export_as_csv.short_description = description
    return export_as_csv