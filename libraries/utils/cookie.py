import datetime
import string
import random

VISITOR_ID_COOKIE_KEY = "visitor_id"
def set_visitor_id_cookie(request,response):
  if not request.COOKIES.has_key(VISITOR_ID_COOKIE_KEY):
    max_age = 365 * 24 * 60 * 60  #one year
    expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
    visitor_id_value = "".join([random.choice(string.letters+string.digits) for x in range(1, 64)]) 
    #response.set_cookie(VISITOR_ID_COOKIE_KEY, visitor_id_value, max_age=max_age, expires=expires, domain=".tarragonawifi.com")
    response.set_cookie(VISITOR_ID_COOKIE_KEY, visitor_id_value, max_age=max_age, expires=expires)
