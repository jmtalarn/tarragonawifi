from django.conf.urls.defaults import *

handler500 = 'djangotoolbox.errorviews.server_error'

# Django Admin
from django.contrib import admin
admin.autodiscover()

# Search for "dbindexes.py" in all installed apps
import dbindexer
dbindexer.autodiscover()

# Search for "search_indexes.py" in all installed apps
import search
search.autodiscover()

urlpatterns = patterns('',
    # Warmup
    ('^_ah/warmup$', 'djangoappengine.views.warmup'),
)
urlpatterns += patterns('',
    # Index
    # (r'^$', 'django.views.generic.simple.direct_to_template', {'template': 'home.html'}),

    # Django Admin
    (r'^admin/', include(admin.site.urls)),
)
#urlpatterns += patterns('home.views',
#    url(r'^$', 'home'),
#    url(r'^about/', 'about'),
#)
urlpatterns += patterns('',
    url(r'^', include('home.urls')),
)
urlpatterns += patterns('',
    url(r'^hotspots/', include('hotspots.urls')),
)
urlpatterns += patterns('',
    url(r'^news/', include('news.urls')),
)

urlpatterns += patterns('',
    url(r'^thumb/', include('scores.urls')),
)

from news.sitemap import NewsSitemap
from hotspots.sitemap import HotspotsSitemap
from home.sitemap import HomeSitemap
from django.conf.urls.defaults import *

sitemaps = {
    'news': NewsSitemap,
    'hotspots': HotspotsSitemap,
    'home': HomeSitemap,

    #'flatpages': FlatPageSitemap,
    #'blog': GenericSitemap(info_dict, priority=0.6),
}
urlpatterns += patterns('',
	url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps})
)

