from django.conf.urls.defaults import *
from news.feeds import NewsFeed

urlpatterns = patterns('news.views',
        url(r'^(?P<article_id>\d+)/$', 'detail'),
        url(r'^list/$','name_search'),
        url(r'^last/(?P<limit>\d+)/$','last_articles'),
        url(r'^feed/$', NewsFeed()),
)