from django.contrib.sitemaps import Sitemap
from news.models import Article

class NewsSitemap(Sitemap):
    #changefreq = "monthly"
    #priority = 0.5
#changefreq and priority are class attributes corresponding to <changefreq> and <priority> elements, respectively. They can be made callable as functions, as lastmod was in the example.
    def items(self):
        return Article.objects.filter(status='P')

    def lastmod(self, obj):
        return obj.date

    def location(self,obj):
        link ="/news/%d" % (obj.id)
        return link
    #By default, location() calls get_absolute_url() on each object and returns the result.

    def changefreq(self, obj):
        #'always'
        #'hourly'
        #'daily'
        #'weekly'
        #'monthly'
        #'yearly'
        #'never'
        return 'monthly'

    def priority(self, obj):
        return 0.8
