from django.contrib.syndication.views import Feed
from news.models import Article

class NewsFeed(Feed):
    title = "Tarragona Wifi. Articles  d'actualitat."
    link = "/news/feed/"
    description = "Tarragona Wifi. Articles  d'actualitat. http://www.tarragonawifi.com"

    def items(self):
        return Article.objects.filter(status='P').order_by('-date')

    def item_title(self, item):
        title ="%s - %s" % (item.title,item.date.strftime("%d/%m/%y"))
        return title

    def item_link(self,item):
        link = "/news/%d" % item.id
        return link

    def item_description(self, item):
        return item.text
        
        