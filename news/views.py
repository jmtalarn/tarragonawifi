# Create your views here.
from django.http import HttpResponse
from search.core import search
from news.models import Article
import logging
from django.utils import simplejson

from django.db.models import Q
from django.shortcuts import get_object_or_404,render_to_response
from django.template import RequestContext

from django.conf import settings

logger = logging.getLogger()

def detail(request, article_id):
    p = get_object_or_404(Article, Q(status='P'), pk=article_id) # ~Q(status='D')
    if (p.lat is None and p.long is None):
       p.lat = 41.1173740568622
       p.long = 1.2500304799316382
    if request.is_ajax() or ( ('HTTP_APPID' in request.META) and request.META['HTTP_APPID']=='mobile-tarragonawifi'):
       mimetype = 'application/javascript'
 
       data = {'article': { 'id': p.id, 
                            'title': p.title, 
                            'tags': p.tags, 
                            'date': p.date.strftime('%A, %d %B %Y, %H:%M'), 
                            'lat': p.lat, 
                            'long': p.long,
                            'text': p.text,
                            'author': p.author,
                            'source': p.source,
                            'source_type': p.source_type} }
       return HttpResponse(simplejson.dumps(data), mimetype)
   ## If you want to prevent non XHR calls
    else:
       return render_to_response('news/article.html', {'article': p}, context_instance=RequestContext(request))

def name_search(request):
   mimetype = 'application/javascript'
   param = request.GET.get('q', False)
   qArray = search(Article, param)
   results = []
   count=0
   qArray.filter(status='P').order_by('-date','title')
   for h in qArray:
      r = {'id': h.pk, 'name': h.title, 'tags': h.tags, 'date': h.date.strftime('%A, %d %B %Y, %H:%M'), 'page': count / (settings.MAX_PER_PAGE) }
      results.append(r)
      count += 1
   max_per_page = len(results) if (len(results)<settings.MAX_PER_PAGE) else settings.MAX_PER_PAGE
   if request.is_ajax() or ( ('HTTP_APPID' in request.META) and request.META['HTTP_APPID']=='mobile-tarragonawifi'):
     data = {'results': results, 'search': param, 'max_per_page': max_per_page }
     return HttpResponse(simplejson.dumps(data), mimetype)
   ## If you want to prevent non XHR calls
   else:
   #  results = qArray;       
     return render_to_response('news/list.html', {'results': results,'search': param, 'request':request,'max_per_page': max_per_page })

def last_articles(request,limit=5):
    lastItems = Article.getLast(limit)
    if request.is_ajax():
       results = []
       for h in lastItems:
           r = {'id': h.pk, 'name': h.title, 'tags': h.tags, 'date': h.date, }
           results.append(r)
       data = {'results': results, 'search': param}
       return HttpResponse(simplejson.dumps(data), mimetype)
     ## If you want to prevent non XHR calls
    else:
       results = lastItems;       
       return render_to_response('news/list.html', {'results': results, 'request':request })


