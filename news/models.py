from django.db import models

class ArticleManager(models.Manager):
    def getLast(self,limit=5):
      return Article.objects.filter(status='P').order_by('date').reverse()[:limit];
      
class Article(models.Model):
    title = models.CharField(max_length=200)
    tags = models.CharField(max_length=100, blank=True, null=True)
    date = models.DateTimeField()
   
    author = models.CharField(max_length=100,blank=True, null=True)
    
    STATUS_CHOICES = (
        ('D', 'Esborrany'), #D Draft
        ('P', 'Publicat'),  #P Published
    )
    SOURCE_TYPE_CHOICES = (
        ('T', 'Text'), #T Text
        ('L', 'Link'),  #L Link
    )
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, blank=False, null= False,default=0)
    
    source_type = models.CharField(max_length=1, choices=SOURCE_TYPE_CHOICES, blank=False, null= False,default=0)
    source = models.CharField(max_length=200,blank=True, null=True)
    
    text = models.TextField(blank=True, null=True)
    long = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)

    def __unicode__(self):
        return "%s" % self.title
        #return u'%s %s %s,%s' % (self.name,self.address,self.lat,self.long)
    
    objects = ArticleManager()
    