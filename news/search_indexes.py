# post.search_indexes
import search
from search.core import startswith
from news.models import Article

# index used to retrieve hotsposts using the name or the address
search.register(Article, ('title', 'tags', ), indexer=startswith)