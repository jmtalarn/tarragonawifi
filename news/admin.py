from news.models import Article
from django.contrib import admin
from libraries.massive.actions import export_as_csv_action
from libraries.massive.actions import save_all_action
from django.http import HttpResponse
from search.core import search


#admin.site.register(Article)
#class ArticleAdmin(admin.ModelAdmin):
#    #fields = ['title','content','pub_date']
#    fieldsets = [
#        (None,           {'fields': ['title','content']}),
#        ('Autoring info',{'fields': ['pub_date','author','category'], 'classes': ['collapse']}),
#    ]
 
class ArticleAdmin(admin.ModelAdmin):
    radio_fields = {"source_type": admin.HORIZONTAL}
    
    fieldsets = (
        ('Article', {
            'fields' : ('title','text','tags','source_type','source')
		}),
        ('Localization', {
            'fields': ('lat','long')
        }),
        ('Publish details', {
            #'classes': ('collapse',),
            'fields': ('status','date','author')
        }),
    )
    
    list_display = ('title','author','date','status' )
    actions = [export_as_csv_action(),save_all_action()]
    #class Media:
        #js = (
		    #'http://yui.yahooapis.com/3.4.1/build/yui/yui-min.js',
            #'http://maps.googleapis.com/maps/api/js?sensor=true',
            #'/js/admin/hotspotDetail.js',
			#'/js/admin/hotspotList.js',
            #)
    #def save_model(self, request, obj, form, change):
    #    obj.source = obj.source.strip().lower();
    #    if ( obj.source.startswith("http://") or
    #         obj.source.startswith("www.") ):
    #        obj.source = '<a href="'+obj.source+'" title="Link to source">'+obj.source+'</a>'
    #    obj.save()    
    

admin.site.register(Article,ArticleAdmin)
#admin.site.add_action(export_as_csv, 'export_as_csv')
